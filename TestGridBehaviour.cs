﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*==== Inspired by https://unitycodemonkey.com/ ====*/

public class TestGridBehaviour : MonoBehaviour {
  // Количество строк сетки
  public int gridRowsCount = 10;
  // Количество колонн сетки
  public int gridColumnsCount = 10;
  // Размер клетки
  public float cellSize = 10f;
  // Выбираем координаты начала сетки
  public Vector3 originPosition = new Vector3(0, 0, 0);

  // Переменная для хранения экземпляра класса Grid
  private Grid grid;

  /**
  * Start
  **/
  private void Start() {
    // Создаём новый экземпляр класса Grid, сохраняем его в переменной
    grid = new Grid(gridRowsCount, gridColumnsCount, cellSize, originPosition);
  }

  /**
  * Update
  **/
  private void Update() {
    GetMouseClicks();
  }

  // Проверяем на клики мышкой
  private void GetMouseClicks() {
    // Клик левой кнопкой мыши
    if (Input.GetMouseButtonDown(0)) {
      var value = Random.Range(100, 200);
      grid.SetCellValue(GetMouseWorldPosition(), value);
    }

    // Клик правой кнопкой мыши
    if (Input.GetMouseButtonDown(1)) {
      var cellValue = grid.GetCellValue(GetMouseWorldPosition());
      Debug.Log($"Right mouse button get {cellValue}");
    }
  }

  // Получить текущие координаты курсора
  private Vector3 GetMouseWorldPosition() {
    Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition); 
    mousePosition.z = 0;
    return mousePosition;
  }

}
